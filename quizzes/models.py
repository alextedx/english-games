from django.db import models

# Create your models here.


class TasksSets(models.Model):
    name = models.CharField(max_length=100, verbose_name="Группа вопросов", unique=True)
    created = models.DateTimeField(verbose_name="Дата создания", auto_now_add=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Группа задач'
        verbose_name_plural = 'Группы задач'


class Tasks(models.Model):
    question = models.CharField(max_length=200, verbose_name="Условие задания")
    is_penalty = models.BooleanField(verbose_name="Штрафной")
    created = models.DateTimeField(verbose_name="Дата создания", auto_now_add=True)
    in_set = models.ForeignKey('TasksSets', on_delete=models.CASCADE,
                               related_name='tasks', verbose_name="В наборе вопросов")

    def __str__(self):
        return self.question

    class Meta:
        verbose_name = 'Задача'
        verbose_name_plural = 'Задачи'


class Words(models.Model):
    word = models.CharField(verbose_name="Слово из предложения", max_length=100)
    created = models.DateTimeField(verbose_name="Дата создания", auto_now_add=True)
    in_task = models.ForeignKey('Tasks', on_delete=models.CASCADE, related_name='words')

    def __str__(self):
        return self.word

    class Meta:
        verbose_name = 'Слово'
        verbose_name_plural = 'Слова'
        ordering = ['id']


class Answers(models.Model):
    answer = models.CharField(verbose_name="Ответ", max_length=100)
    created = models.DateTimeField(verbose_name="Дата создания", auto_now_add=True)
    in_task = models.ForeignKey('Tasks', on_delete=models.CASCADE, related_name='answers')

    def __str__(self):
        return self.answer

    class Meta:
        verbose_name = 'Ответ'
        verbose_name_plural = 'Ответы'
