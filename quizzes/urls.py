from django.urls import include, path
from . import views
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'sets', views.SetsViewSet)
router.register(r'tasks', views.TasksViewSet)

urlpatterns = [
    path('', include(router.urls))
]
