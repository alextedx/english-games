from django.contrib import admin
import nested_admin
from guardian.admin import GuardedModelAdmin
from .models import Tasks, TasksSets, Words, Answers


# Register your models here.


class WordsInline(nested_admin.NestedTabularInline):
    model = Words


class AnswersInline(nested_admin.NestedTabularInline):
    model = Answers
    min_num = 1
    extra = 1


class TasksInline(nested_admin.NestedTabularInline):
    model = Tasks
    inlines = [WordsInline, AnswersInline]
    list_filter = ['in_set', 'is_penalty', 'created']
    search_fields = ['question']
    list_display = ('question', 'created', 'in_set')


class TasksSetsAdmin(GuardedModelAdmin, nested_admin.NestedModelAdmin):
    model = TasksSets
    inlines = [TasksInline]


admin.site.register(TasksSets, TasksSetsAdmin)
