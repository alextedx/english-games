from rest_framework.viewsets import ReadOnlyModelViewSet
from .models import TasksSets, Tasks
from .serializers import SetsSerializer, TasksSerializer, SetSerializer
from .pagination import QuizzesSetPagination
from .permissions import QuizzesModelPermissions, QuizzesObjectPermissions
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework_guardian.filters import ObjectPermissionsFilter


class SetsViewSet(ReadOnlyModelViewSet):
    """
    This viewset automatically provides `list` and `detail` actions.
    """
    permission_classes = (QuizzesModelPermissions, )
    queryset = TasksSets.objects.all()
    serializer_class = SetsSerializer
    pagination_class = QuizzesSetPagination

    def get_serializer_class(self):
        print(self.action)
        if hasattr(self, 'action'):
            if self.action == 'list':
                return SetsSerializer
            elif self.action == 'retrieve':
                return SetSerializer
        return self.serializer_class


class TasksViewSet(ReadOnlyModelViewSet):
    """
    This viewset automatically provides `list` and `detail` actions.
    """
    queryset = Tasks.objects.all()
    serializer_class = TasksSerializer
    permission_classes = (QuizzesObjectPermissions,)
    filter_backends = [ObjectPermissionsFilter, DjangoFilterBackend]
    pagination_class = QuizzesSetPagination
    filterset_fields = ('in_set', 'is_penalty')
