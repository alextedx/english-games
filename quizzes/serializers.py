from rest_framework import serializers
from .models import TasksSets, Tasks, Words, Answers


class WordsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Words
        fields = ('word',)


class AnswersSerializer(serializers.ModelSerializer):
    class Meta:
        model = Answers
        fields = ('answer',)

    def to_representation(self, instance):
        """Convert 'answer' to lowercase."""
        ret = super().to_representation(instance)
        ret['answer'] = ret['answer'].lower()
        return ret


class TasksSerializer(serializers.ModelSerializer):
    words = WordsSerializer(many=True, read_only=True)
    answers = AnswersSerializer(many=True, read_only=True)

    class Meta:
        model = Tasks
        fields = ('in_set', 'is_penalty', 'question', 'words', 'answers',)


class SetsSerializer(serializers.HyperlinkedModelSerializer):
    # tasks = serializers.HyperlinkedRelatedField(many=True, view_name='tasks-detail', read_only=True)

    class Meta:
        model = TasksSets
        fields = ('url', 'id', 'name')


class SetSerializer(serializers.HyperlinkedModelSerializer):
    # tasks = serializers.HyperlinkedRelatedField(many=True, view_name='tasks-detail', read_only=True)

    class Meta:
        model = TasksSets
        fields = ('url', 'id', 'name', 'tasks')
        depth = 1
