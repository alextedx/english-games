from rest_framework import viewsets, mixins
from .models import CustomUser
from .serializers import AvailableGamesSerializer


# Create your views here.

class AvailableGamesViewSet(mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    queryset = CustomUser.objects.all()
    serializer_class = AvailableGamesSerializer
