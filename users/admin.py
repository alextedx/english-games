from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import CustomUser


class CustomUserAdmin(UserAdmin):

    model = CustomUser
    fieldsets = [
        ('Персональные данные', {'fields': ['first_name',
                                            'last_name',
                                            'email',
                                            'username',
                                            'password',
                                            'english_level',
                                            ]}),
        ('Информация об аккаунте', {'fields': ['is_superuser',
                                               'is_staff',
                                               'groups',
                                               'user_permissions',
                                               # 'userobjectpermission_set',
                                               ]}),
        ('Метаинформация', {'fields': ['last_login', 'is_active', 'date_joined']}),
    ]
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('first_name', 'last_name', 'username', 'email', 'password1', 'password2'),
        }),
    )
    list_display = ['username', 'first_name', 'last_name', 'email']
    search_fields = ('username', 'email', 'first_name', 'last_name')
    ordering = ('first_name', 'last_name', 'email',)


admin.site.register(CustomUser, CustomUserAdmin)
