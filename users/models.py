from django.db import models
from django.contrib.auth.models import AbstractUser


class CustomUser(AbstractUser):
    """ Дополнительные атрибуты пользователя сайта """
    level_choices = (
        ("Beginner", 'начальный'),
        ("Elementary English", 'элементарный '),
        ("Intermediate English", 'средний'),
        ("Upper-Intermediate English", 'средне-продвинутый'),
        ("Advanced English", 'продвинутый'),
        ("Proficiency English", 'Владение в совершенстве'),
    )
    english_level = models.CharField(verbose_name="Уровень знания языка", blank=True, max_length=30,
                                     default="Beginner", choices=level_choices)

    def __str__(self):
        return f"{self.username}, {self.email}"
