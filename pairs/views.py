from rest_framework import viewsets
from .permissions import PairsModelPermissions, PairsObjectPermissions
from rest_framework_guardian import filters
from .models import Deck
from .serializers import DeckSerializer


class DeckViewSet(viewsets.ReadOnlyModelViewSet):
    """
    This viewset automatically provides `list` and `detail` actions.
    """
    queryset = Deck.objects.all()
    serializer_class = DeckSerializer
    permission_classes = (PairsObjectPermissions, PairsModelPermissions)
    filter_backends = [filters.ObjectPermissionsFilter]
