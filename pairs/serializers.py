from rest_framework import serializers
from .models import Deck, Card


class CardSerializer(serializers.ModelSerializer):
    class Meta:
        model = Card
        fields = ('in_deck', 'word', 'picture', 'created')


class DeckSerializer(serializers.ModelSerializer):
    cards = CardSerializer(many=True, read_only=True)

    class Meta:
        model = Deck
        fields = ('name', 'picture', 'cards')
