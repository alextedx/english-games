from django.db import models
from PIL import Image, ImageOps
from io import BytesIO
from django.core.files.uploadedfile import InMemoryUploadedFile
import sys


# Create your models here.
# func to resize uploaded picture

def resize_picture(crude_object, width=None, height=None, qual=90):
    if crude_object.picture:
        # Opening the uploaded image
        img = Image.open(crude_object.picture)
        if img.mode != 'RGB':
            img = img.convert('RGB')
        output = BytesIO()
        # Resize/modify the image
        if height is None:
            base_width = width
            width_percent = (base_width / float(img.size[0]))
            height_size = int((float(img.size[1]) * float(width_percent)))
            img = ImageOps.fit(img, (width, height_size), Image.LANCZOS)
        elif width is None:
            base_height = height
            height_percent = (base_height / float(img.size[1]))
            width_size = int((float(img.size[0]) * float(height_percent)))
            img = ImageOps.fit(img, (width_size, height), Image.LANCZOS)
        else:
            img = ImageOps.fit(img, (width, height), Image.LANCZOS)
        # after modifications, save it to the output
        img.save(output, 'JPEG', quality=qual)
        output.seek(0)
        # change the imagefield value to be the newley modifed image value
        crude_object.picture = InMemoryUploadedFile(
            output,
            'ImageField',
            "%s.jpg" % crude_object.picture.name.split('.')[0],
            'image/jpeg',
            sys.getsizeof(output),
            None
        )
    return crude_object
    

class Deck(models.Model):
    name = models.CharField(verbose_name="Название колоды", max_length=100)
    created = models.DateTimeField(verbose_name="Дата создания", auto_now_add=True)
    picture = models.ImageField(
        verbose_name="Рубашка карт в колоде",
        upload_to='pictures/backside/',
        blank=True,
        default='pictures/backside/default_backside.jpg'
    )

    class Meta:
        ordering = ['-id']
        verbose_name = 'Колода'
        verbose_name_plural = 'Колоды'

    def __str__(self):
        return '%s, %s' % (self.id, self.name)

    def save(self):
        super(Deck, resize_picture(self, 854, 480)).save()


class Card(models.Model):
    in_deck = models.ForeignKey('Deck', on_delete=models.CASCADE, related_name='cards')
    word = models.CharField(verbose_name="Ассоциированное слово", max_length=100, blank=True)
    picture = models.ImageField(
        verbose_name="Изображение",
        upload_to='pictures/frontside/',
        blank=True,
    )
    created = models.DateTimeField(verbose_name="Дата создания", auto_now_add=True)

    class Meta:
        verbose_name = 'Карта'
        verbose_name_plural = 'Карты'

    def save(self):
        super(Card, resize_picture(self, 854, 480)).save()
