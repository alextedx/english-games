from django.apps import AppConfig


class PairsConfig(AppConfig):
    name = 'pairs'
    verbose_name = 'Картинки и значения'
