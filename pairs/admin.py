from django.contrib import admin
from .models import Deck, Card
from django.utils.html import mark_safe
from guardian.admin import GuardedModelAdmin


# Register your models here.

class CardInline(admin.TabularInline):
    model = Card

    def get_card_picture(self, obj):
        if obj.picture:
            return mark_safe('<img src="{url}" alt="Здесь должна быть картинка" height="250" width="250"/>'.format(
                url=obj.picture.url
            ))
        return mark_safe('<img src="" alt="Здесь должна быть картинка" height="250" width="250"/>')

    get_card_picture.short_description = 'Изображение'
    readonly_fields = ['get_card_picture']


class DeckInline(GuardedModelAdmin):
    inlines = [
        CardInline
    ]
    list_filter = ['created', 'name']
    search_fields = ['name']

    def get_backside_picture(self, obj):
        if obj.picture:
            return mark_safe('<img src="{url}" alt="Здесь должна быть картинка" height="250" width="250"/>'.format(
                url=obj.picture.url
            ))
        return mark_safe('<img src="" alt="Здесь должна быть картинка" height="250" width="250"/>')

    get_backside_picture.short_description = 'Изображение'
    list_display = (
        'name',
        'get_backside_picture',
        'created',
    )
    readonly_fields = ['get_backside_picture']


admin.site.register(Deck, DeckInline)
