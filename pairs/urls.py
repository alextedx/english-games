from django.urls import include, path
from . import views
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'deck', views.DeckViewSet)

urlpatterns = [
    path('', include(router.urls))
]
