from django.contrib import admin
from .models import SixCardsDeck, SixCardsCard
from django.utils.html import mark_safe
from guardian.admin import GuardedModelAdmin


# Register your models here.

class SixCardsCardInline(admin.TabularInline):
    model = SixCardsCard

    def get_card_picture(self, obj):
        if obj.picture_thumbnail:
            return mark_safe('<img src="{url}" alt="Здесь должна быть картинка" height="250" width="250"/>'.format(
                url=obj.picture_thumbnail.url
            ))
        return mark_safe('<img src="" alt="Здесь должна быть картинка" height="250" width="250"/>')

    get_card_picture.short_description = 'Изображение'
    readonly_fields = ['get_card_picture']


class SixCardsDeckInline(GuardedModelAdmin):
    inlines = [
        SixCardsCardInline
    ]
    list_filter = ['created', 'name']
    search_fields = ['name', ]

    def get_backside_picture(self, obj):
        if obj.card_back_thumbnail:
            return mark_safe('<img src="{url}" alt="Здесь должна быть картинка" height="250" width="250"/>'.format(
                url=obj.card_back_thumbnail.url
            ))
        return mark_safe('<img src="" alt="Здесь должна быть картинка" height="250" width="250"/>')

    def get_frontside_background_picture(self, obj):
        if obj.frontside_background_picture_thumbnail:
            return mark_safe('<img src="{url}" alt="Здесь должна быть картинка" height="250" width="250"/>'.format(
                url=obj.frontside_background_picture_thumbnail.url
            ))
        return mark_safe('<img src="" alt="Здесь должна быть картинка" height="250" width="250"/>')

    get_backside_picture.short_description = 'Изображение'
    list_display = (
        'name',
        'get_backside_picture',
        'created',
    )
    readonly_fields = ['get_backside_picture', 'get_frontside_background_picture', ]


admin.site.register(SixCardsDeck, SixCardsDeckInline)
