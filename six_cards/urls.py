from django.urls import include, path
from . import views
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'six_cards_decks', views.SixCardsDeckViewSet)

urlpatterns = [
    path('', include(router.urls))
]
