from rest_framework import viewsets
from rest_framework_guardian import filters
from .models import SixCardsDeck
from .serializers import DeckSerializer
from .permissions import SixCardsModelPermissions, SixCardsObjectPermissions


# Create your views here.

class SixCardsDeckViewSet(viewsets.ReadOnlyModelViewSet):
    """
    This viewset automatically provides `list` and `detail` actions.
    """
    queryset = SixCardsDeck.objects.all()
    serializer_class = DeckSerializer
    permission_classes = (SixCardsObjectPermissions, )
    filter_backends = [filters.ObjectPermissionsFilter]
