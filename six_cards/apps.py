from django.apps import AppConfig


class SixCardsConfig(AppConfig):
    name = 'six_cards'
    verbose_name = 'Шесть карт'
