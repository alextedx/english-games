from django.db import models
from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFit
from PIL import Image, ImageOps
from io import BytesIO
from django.core.files.uploadedfile import InMemoryUploadedFile
import sys


def resize_picture(width=None, height=None, *args):
    for crude_object in args:
        name = crude_object.name
        # Opening the uploaded image
        img = Image.open(crude_object)
        img_format = img.format
        mimetype = img.get_format_mimetype()
        # if img.mode != 'RGB':
        #     img = img.convert('RGB')
        output = BytesIO()
        # Resize/modify the image
        if height is None:
            base_width = width
            width_percent = (base_width / float(img.size[0]))
            height_size = int((float(img.size[1]) * float(width_percent)))
            img = ImageOps.fit(img, (width, height_size), Image.ANTIALIAS)
        elif width is None:
            base_height = height
            height_percent = (base_height / float(img.size[1]))
            width_size = int((float(img.size[0]) * float(height_percent)))
            img = ImageOps.fit(img, (width_size, height), Image.ANTIALIAS)
        else:
            img = ImageOps.fit(img, (width, height), Image.ANTIALIAS)
        # after modifications, save it to the output
        img.save(output, img_format)
        output.seek(0)
        # change the imagefield value to be the newley modifed image value
        crude_object = InMemoryUploadedFile(output, 'ImageField', name,
                                            mimetype, sys.getsizeof(output), None
                                            )
    return None


# Create your models here.


class SixCardsDeck(models.Model):
    name = models.CharField(verbose_name="Название колоды", max_length=100)
    created = models.DateTimeField(verbose_name="Дата создания", auto_now_add=True)
    card_back = models.ImageField(
        verbose_name="Рубашка карт в колоде",
        upload_to='pictures/backside/',
        blank=True,
        default='pictures/backside/default_backside.jpg'
    )
    card_back_thumbnail = ImageSpecField(
        source='card_back',
        processors=[ResizeToFit(width=480, )],
        format='JPEG',
        options={'quality': 75})
    frontside_background_picture = models.ImageField(
        verbose_name="Фон карт",
        upload_to='pictures/background/',
        blank=True,
    )
    frontside_background_picture_thumbnail = ImageSpecField(
        source='frontside_background_picture',
        processors=[ResizeToFit(width=480, )],
        format='JPEG',
        options={'quality': 75})

    class Meta:
        ordering = ['-id']
        verbose_name = 'Колода'
        verbose_name_plural = 'Колоды'

    def __str__(self):
        return '%s, %s' % (self.id, self.name)

    def save(self, *args, **kwargs):
        if self.card_back:
            resize_picture(854, 480, self.card_back)
        if self.frontside_background_picture:
            resize_picture(854, 480, self.frontside_background_picture)
        super().save(*args, **kwargs)


class SixCardsCard(models.Model):
    in_deck = models.ForeignKey(
        'SixCardsDeck',
        on_delete=models.CASCADE,
        related_name='cards', )
    text = models.TextField(verbose_name="Ассоциированное слово", blank=True)
    picture = models.ImageField(
        verbose_name="Изображение",
        upload_to='pictures/frontside/',
        blank=True,
    )
    picture_thumbnail = ImageSpecField(
        source='picture',
        processors=[ResizeToFit(width=480, )],
        format='JPEG',
        options={'quality': 75})
    created = models.DateTimeField(verbose_name="Дата создания", auto_now_add=True)

    class Meta:
        verbose_name = 'Карта'
        verbose_name_plural = 'Карты'

    def __str__(self):
        return '%s, %s' % (self.id, self.text)

    def save(self, *args, **kwargs):
        if self.picture:
            resize_picture(854, 480, self.picture)
        super().save(*args, **kwargs)
