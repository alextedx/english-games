from rest_framework import serializers
from .models import SixCardsDeck, SixCardsCard


class CardSerializer(serializers.ModelSerializer):
    picture_thumbnail = serializers.ImageField(read_only=True)

    class Meta:
        model = SixCardsCard
        fields = ('text', 'picture_thumbnail')


class DeckSerializer(serializers.ModelSerializer):
    card_back_thumbnail = serializers.ImageField(read_only=True)
    frontside_background_picture_thumbnail = serializers.ImageField(read_only=True)
    cards = CardSerializer(many=True, read_only=True)

    class Meta:
        model = SixCardsDeck
        fields = ('name', 'card_back_thumbnail', 'frontside_background_picture_thumbnail', 'cards')
