# pull official base image
FROM python:alpine



# set environment varibles
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# set work directory
WORKDIR /srv/linguafromhome

# copy project
COPY . ./

# install system libs for psycopg and pillow
RUN apk add --no-cache postgresql-dev jpeg-dev zlib-dev
# install dependencies

RUN apk add --no-cache --virtual .build-deps build-base linux-headers \
&& pip install --no-cache-dir -U pip setuptools \
&& pip install --no-cache-dir -r requirements.txt \
&& pip install --no-cache-dir gunicorn psycopg2 \
&& apk del .build-deps

# set user
RUN chmod o+x boot.sh
#RUN chown -R svc_linguafromhome:svc_linguafromhome ./
#USER svc_linguafromhome

ENTRYPOINT ["./boot.sh"]
